package model.entity;

import javax.persistence.*;

@Entity
@Table(name = "author_article", schema = "public")
public class AuthorArticle {
    private Integer id;
    private Integer id_article;
    private Integer id_author;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_article")
    public Integer getTitle() {
        return id_article;
    }

    public void setTitle(Integer id_article) {
        this.id_article = id_article;
    }

    @Basic
    @Column(name = "id_author")
    public Integer getDescription() {
        return id_author;
    }

    public void setDescription(Integer id_author) {
        this.id_author = id_author;
    }

    @Override
    public String toString() {
        return "AuthorArticle {" +
                "id=" + id +
                ", id_article='" + id_article + '\'' +
                ", id_author='" + id_author + '\'' +
                '}';
    }

}

