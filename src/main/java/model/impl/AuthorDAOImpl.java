package model.impl;

import model.AuthorDAO;
import model.entity.Author;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

public class AuthorDAOImpl implements AuthorDAO {

    private static final EntityManager entityManager = Persistence.createEntityManagerFactory("public").createEntityManager();

    @Transactional
    @Override
    public List<Author> authors() {
        return entityManager.createQuery("SELECT author FROM Author author").getResultList();
    }

    @Transactional
    @Override
    public boolean createAuthor(Author author) {
        System.out.println("============");
        author.setId(3);
        System.out.println(author.toString());
        entityManager.getTransaction().begin();
        entityManager.persist(author);
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public boolean readAuthor(Author author) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("SELECT author FROM Author author WHERE author.id = :id");
        query.setParameter("id", author.getId());
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public boolean updateAuthor(Author author) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("UPDATE Author a SET a.name = :name, a.surname = :surname, a.patronymic = :patronymic, a.university = :university "
                + "WHERE a.id = :id");
        query.setParameter("id", author.getId());
        query.setParameter("name", author.getName());
        query.setParameter("surname", author.getSurname());
        query.setParameter("patronymic", author.getPatronymic());
        query.setParameter("university", author.getUniversity());
        entityManager.getTransaction().commit();
        return true;
    }

    @Transactional
    @Override
    public boolean deleteAuthor(Author author) {
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("DELETE FROM Author a WHERE a.id = :id");
        query.setParameter("id", author.getId());
        entityManager.getTransaction().commit();
        return true;
    }

}