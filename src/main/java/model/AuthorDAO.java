package model;

import model.entity.Author;

import java.util.List;

public interface AuthorDAO {

    List<Author> authors();

    boolean createAuthor(Author author);

    boolean readAuthor(Author author);

    boolean updateAuthor(Author author);

    boolean deleteAuthor(Author author);

}
