package managedBeans;

import model.AuthorDAO;
import model.entity.Author;
import model.impl.AuthorDAOImpl;
import org.primefaces.PrimeFaces;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean
@SessionScoped
public class AuthorsManagedBean {
    private AuthorDAO dao;

    List<Author> authors;
    Author current;
//    int prescriptionsOfAuthor;

    public AuthorsManagedBean() {
        this.dao = new AuthorDAOImpl();
        this.authors = dao.authors();
    }

    public List<Author> authors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public Author getCurrent() {
        return current;
    }

    public void setCurrent(Author current) {
        this.current = current;
    }

    public void acceptEditAuthor() {
        dao.updateAuthor(current);
        this.authors = dao.authors();
        PrimeFaces.current().executeScript("PF('editDialog').hide()");
    }

    public void addNewAuthor() {
        this.current = new Author();
    }

    public void acceptNewAuthor() {
        dao.createAuthor(current);
        this.authors = dao.authors();
        PrimeFaces.current().executeScript("PF('addDialog').hide()");
    }

    public void removeAuthor(Author author) {
        dao.deleteAuthor(author);
        this.authors = dao.authors();
    }

    public String goToHome() {
        return "index";
    }

    public String goToAuthor() {
        return "author";
    }

    public String goToArticle() {
        return "article";
    }

    public String goToArticleAuthor() {
        return "article-author";
    }

}
